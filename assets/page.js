(function() {
  var f, c, e, b, d, i, a, h, g = {}.hasOwnProperty;
  c = null;
  i = null;
  b = {};
  f = false;
  e = false;
  a = _.throttle(function() {
    var j, k;
    if (f) {
      e = true;
      return
    } else {
      e = false
    }
    f = true;
    c.revert(false);
    for (j in b) {
      if (!g.call(b, j)) {
        continue
      }
      k = b[j];
      k = parseFloat(k, 10);
      if (k === 0) {
        continue
      }
      c[j](k);
    }
    return c.render(function() {
      f = false;
      console.log(a);
      if (e) {
        return a()
      }
    })
  },
  300);
  d = false;
  h = function(k) {
    var l, j;
    if (d) {
      return
    }
    $("#PresetFilters a").removeClass("Active");
    l = $("#PresetFilters a[data-preset='" + k + "']");
    j = l.html();
    l.addClass("Active").html("Rendering...");
    d = true;
    i.revert(false);
    i[k]();
    return i.render(function() {
      l.html(j);
      return d = false
    })
  };
  $(document).ready(function() {
    if (! ($("#example").length > 0)) {
      return
    }
    c = Caman("#example");
    i = Caman("#preset-example");
    $(".FilterSetting input").each(function() {
      var j;
      j = $(this).data("filter");
      return b[j] = $(this).val()
    });
    $("#Filters").on("change", ".FilterSetting input",
    function() {
      var j, k;
      j = $(this).data("filter");
      k = $(this).val();
      b[j] = k;
      console.log(b);
      $(this).find("~ .FilterValue").html(k);
      return a()
    });
    return $("#PresetFilters").on("click", "a",
    function() {
      return h($(this).data("preset"))
    })
  })
}).call(this); 

(function() {
  var a;
  a = function(d) {
    var b, c, e;
    e = d.attr("id");
    d.attr("id", "");
    c = $("<div>").css({
      position: "absolute",
      visibility: "hidden",
      top: $(document).scrollTop() + "px"
    }).attr("id", e).appendTo(document.body);
    document.location.hash = "#" + e;
    c.remove();
    d.attr("id", e);
    b = $("#GuideSections li > a").filter("[href=#" + (d.attr("id")) + "]");
    b.parents("ul").find(".Active").removeClass("Active");
    return b.parents("li").addClass("Active")
  };
  $(document).ready(function() {
    var b;
    $("#GuideSections").on("click", "a",
    function() {
      var c, d;
      c = $($(this).attr("href"));
      d = Math.max(0, c.position().top - 129);
      document.location.hash = $(this).attr("href");
      setTimeout(function() {
        return $("body").scrollTop(d)
      },
      50);
      return false
    });
    b = _.map($("#GuideSections li > a"),
    function(c) {
      return $($(c).attr("href"))
    });
    b = b.reverse();
    return $(document).on("scroll", _.throttle(function() {
      var e, f, d, c;
      f = $(document).scrollTop();
      for (d = 0, c = b.length; d < c; d++) {
        e = b[d];
        if (f >= e.position().top - 130) {
          a(e);
          return
        }
      }
    },
    200))
  })
}).call(this);